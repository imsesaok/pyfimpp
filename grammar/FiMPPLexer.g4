/*This file is part of PyFiMPP.
*
*   PyFiMPP is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   PyFiMPP is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with PyFiMPP.  If not, see <http://www.gnu.org/licenses/>.
*/
lexer grammar FiMPPLexer;

//keywords
IMPORT: 'Remember when I wrote about';
VARIABLE: 'Did you know that';
REPORT: 'Dear';
INITIALIZATION_CONSTANT: 'always ' Initialization ;
INITIALIZATION_VARIABLE: 'like' | 'likes'; //some overlapping keywords are removed
REPORT_END: 'Your faithful student';
EXECUTE: 'I ' ( 'remembered' | 'would' );
TODAY: 'Today';
PARAGRAPH: 'I learned';
RETURN_TYPE: 'with' | 'as';
PARAMETER: 'using';
RETURN: 'Then you get';
PARAGRAPH_END: 'That' SingleQuote 's all about';
IF_LEFT: 'If' | 'When';
IF_RIGHT: 'then';
ELSE: 'Or Else' | 'Otherwise';
END_IF: 'That' SingleQuote 's what I would do';
SWITCH: 'In regards to';
CASE_LEFT: 'On the';
CASE_RIGHT: ( 'nd' | 'rd' | 'st' | 'th' ) ? ' hoof';
DEFAULT: 'If all else fails';
END_SWITCH_WHILE: 'That' SingleQuote 's what I did';
WHILE: 'While' | 'As long as';
DO_WHILE: 'Here' SingleQuote 's what I did';
END_DO_WHILE: 'I did this ' ( 'as long as' | 'while' );
FOR_EACH: 'For every';
PRINT: 'I ' ( 'said' | 'wrote' | 'thought' | 'sang' );
INPUT: 'I ' ( 'heard' | 'read' );
PROMPT: 'I asked';
ASSIGN: 'are now' | 'becomes' | 'become' | 'is now' | 'now likes' | 'now like';

//operators
GREATER_THAN: Equal ' ' Greater ' than';
LESS_THAN: Equal ' less than';
GREATER_THAN_OR_EQUAL: Equal Negative ' less than';
LESS_THAN_OR_EQUAL: Equal Negative ' ' Greater ' than' ;
NOT_EQUAL: Equal Negative;
EQUAL: 'had' | 'were'; //some overlapping keywords are removed
TRUE: 'correct' | 'right' | 'true' | 'yes';
FALSE: 'false' | 'incorrect' | 'no' | 'wrong';
AND: 'and';
XOR_LEFT: 'either';
OR: 'or';
NOT: 'not';
INCREMENT: 'got one more';
DECREMENT: 'got one less';
INCREMENT_LEFT: 'There was one more';
DECREMENT_LEFT: 'There was one less';
ADDITION: 'added to' | 'plus';
SUBTRACTION: 'minus' | 'without' ;
MULTIPLICATION: 'times' | 'multiplied with' ;
DIVISION: 'divided by';
ADDITION_LEFT: 'add';
SUBTRACTION_LEFT: 'subtract' | 'the difference between';
MULTIPLICATION_LEFT: 'multiply' | 'the product of';
DIVISION_LEFT: 'divide';
BY: 'by';
FROM: 'from';
TO: 'to';
IN: 'in';

//literal type names
NUMBER: SinglePrefix ? 'number';
NUMBERS: MultiPrefix ? 'number' MultiSuffix;
BOOLEAN: SinglePrefix ? BooleanName;
BOOLEANS: MultiPrefix ? BooleanName MultiSuffix;
CHARACTER: SinglePrefix ? CharacterName;
STRING: SinglePrefix ? ( CharacterName MultiSuffix | StringName );
STRINGS: MultiPrefix ? ( CharacterName | StringName ) MultiSuffix;

//literal values
NumberLiteral: Minus ? Integer ('.' Integer ) ?;
CharacterLiteral: SingleQuote Character SingleQuote;
StringLiteral: DoubleQuote String +? DoubleQuote;
NullLiteral: 'nothing';

//overlapping keywords
OVERLAP_INIT_EQUAL: 'has' | 'is' | 'was';

fragment Character
    : ~ ['‘’]
    ;

fragment String
    : ~ ["”“]
    ;

fragment SingleQuote
    : '\''
    | '‘'
    | '’'
    ;

fragment DoubleQuote
    : '"'
    | '”'
    | '“'
    ;

fragment Minus
    : '-'
    ;

fragment Initialization
    : 'has'
    | 'is'
    | 'like'
    | 'likes'
    | 'was'
    ;

fragment BooleanName
    : 'argument'
    | 'logic'
    ;

fragment CharacterName
    : 'character'
    | 'letter'
    ;

fragment StringName
    : 'phrase'
    | 'quote'
    | 'sentence'
    | 'word'
    ;

fragment SinglePrefix //how should i name it?
    : ( 'a' | 'an' | 'the' ) ' '
    ;

fragment MultiPrefix //how should i name it?
    : ( 'many' | 'the' ) ' '
    ;

fragment MultiSuffix //how should i name it?
    :'e'? 's'
    ;

fragment Equal
    : 'had'
    | 'has'
    | 'is'
    | 'was'
    | 'were'
    ;

fragment Negative
    : ' not'
    | ' no'
    | 'n'SingleQuote't'
    ;

fragment Greater
    : 'more'
    | 'greater'
    ;

fragment Integer
    : Digit +
    ;

fragment Digit
    : [0-9]
    ;


WHITESPACE
    : [\r\n\f\t ] -> skip
    ;

PNCT //Name got shortened since it's used so widely.
    : [.,?:…‽!]
    ;

Comment
    : 'P.' + 'S.' ~ [\r\n] * -> skip
    ;

MultilineComment
    : '(' ~ [)] * ?  ')' -> skip
    ;

Identifier
    : Letter LetterOrDigit *
    ;

Name
    : LetterOrDigit +
    ;

fragment Letter
    : ~ [ 0-9.,?:…‽!"\n\r\t\f]
    ;

fragment LetterOrDigit
    : Letter
    | Digit
    ;

