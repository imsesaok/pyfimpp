# encoding: utf-8
#This file is part of PyFiMPP.
#
#   PyFiMPP is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyFiMPP is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyFiMPP.  If not, see <http://www.gnu.org/licenses/>.

import ply.lex as lex
import re

tokens = [ # Defines tokens
    # Keywords #
    'ASSIGN',
    'CASE',
    'CONST',
    'DEFAULT',
    'DO_WHILE',
    'END_DID',
    'END_DO_WHILE',
    'END_PARAG',
    'END_WOULD_DO',
    'ELSE',
    'FOR_EACH',
    'HOOF',
    'IF',
    'IMPORT',
    'INPUT',
    'LIKE',
    'MANE',
    'PARAGRAPH',
    'PARAMS',
    'PRINT',
    'PROMPT',
    'REP',
    'REP_END',
    'RETURN',
    'RUN',
    'SWITCH',
    'THEN',
    'TO_GET',
    'VAR',
    'WHILE',
    
    # Operators #
    'ADD',
    'ADD_PREF',
    'DEC',
    'DEC_PREF',
    'DIV',
    'DIV_PREF',
    'EQUAL',
    'GREATER_THAN',
    'GR_THAN_OR_EQ',
    'INC',
    'INC_PREF',
    'LESS_THAN',
    'LS_THAN_OR_EQ',
    'MUL',
    'MUL_PREF',
    'NOT_CASE',
    'NOT_EQUAL',
    'OR',
    'SUB',
    'SUB_PREF',
    'XOR',
    
    # Keyword with Multiple Usage #
    'WAS',
    'AND',
    'HAS',
    'IS',
    'BY',
    'FROM',
    'NOT',
    'TO',
    
    # Types #
    
    'NUMBER',
    'NUMBERS',
    'BOOLEAN',
    'BOOLEANS',
    'CHARACTER',
    'STRING',
    'STRINGS',
    
    # Literals #
    
    'Number',
    'Character',
    'String',
    'True',
    'False',
    'Null',
    'Punctuation',
    
    # Ignored # 
    
    'SLComment',
    'MLComment',
    'WHITESPACE',
    
    # ID #
    
    'ID'
]

# Define Fragments of Expressions to Make Things More Simple
Char    = r"[^'‘’]"
String  = r'[^"”“]'
SQuote  = r"['‘’]"
DQuote  = r'["”“]'

Bool    = r'(argument|logic)'
Char    = r'character|letter'
Str     = r'phrase|quote|sentence|word'
SPref   = r'((a|the)\s)'
MPref   = r'((many|the)\s)'
Equal   = r'(ha(d|s)|is|was|were)'
Neg     = r'(\s not|n' + SQuote + r't)'
NotEq   = r'(' + Equal + r'\s no|(is|was|were)' + Neg + r')'
Greater = r'(more|greater)'

# Keywords #
t_ASSIGN        = r'\b ((are|is) \s now|becomes?|now \s likes?) \b'
t_CASE          = r'\b On \s the \b'
t_CONST         = r'\b always \s (has|is|likes?|was) \b'
t_DEFAULT       = r'\b If \s all \s else \s fails \b'
t_DO_WHILE      = r'\b Here' + SQuote + r's \s what \s I \s did \b'
t_END_DID       = r'\b That' + SQuote + r's \s what \s I \s did \b'
t_END_DO_WHILE  = r'\b I \s did \s this \s (as \s long \s as|while) \b'
t_END_PARAG     = r'\b That' + SQuote + r's \s all \s about \b'
t_END_WOULD_DO  = r'\b That' + SQuote + r's \s what \s I \s would \s do \b'
t_ELSE          = r'\b (Or \s else|Otherwise) \b'
t_FOR_EACH      = r'\b For \s every \b'
t_HOOF          = r'\b (st|nd|rd|th)? \s hoof \b'
t_IF            = r'\b (If|When) \b'
t_IMPORT        = r'\b Remember \s when \s I \s wrote \s about \b'
t_INPUT         = r'\b I \s (hear|rea)d \b'
t_LIKE          = r'\b likes? \b'
t_MANE          = r'\b Today \s I \s learned \b'
t_PARAGRAPH     = r'\b I \s learned \b'
t_PARAMS        = r'\b using \b'
t_PRINT         = r'\b I \s (sa(id|ng)|wrote) \b'
t_PROMPT        = r'\b I \s asked \b'
t_REP           = r'\b Dear \b'
t_REP_END       = r'\b Your \s faithful \s student \b'
t_RUN           = r'\b I \s (remembere|woul)d \b'
t_RETURN        = r'\b Then \s you \s get \b'
t_SWITCH        = r'\b In \s regards \s to \b'
t_TO_GET        = r'\b (to \s get|with) \b'
t_THEN          = r'\b then \b'
t_VAR           = r'\b Did \s you \s know \s that \b'
t_WHILE         = r'\b (As \s long \s as|while) \b'
    
# Operators #
t_ADD           = r'\b (added \s to|plus) \b'
t_ADD_PREF      = r'\b add \b'
t_DEC           = r'\b got \s one \s less \b'
t_DEC_PREF      = r'\b (There \s was \s one \s less|subtract) \b'
t_DIV           = r'\b divided \s by \b'
t_DIV_PREF      = r'\b divide \b'
t_EQUAL         = r'\b (had|were) \b'
t_GREATER_THAN  = r'\b ((is|was|were) \s' + Greater + r'| ha(d|s) \s more) \s than \b'
t_GR_THAN_OR_EQ = r'\b ' + NotEq + r' \s less \s than \b'
t_INC           = r'\b got \s one \s more \b'
t_INC_PREF      = r'\b (There \s was \s one \s more|add) \b'
t_LESS_THAN     = r'\b' + Equal + r' \s less \s than \b'
t_LS_THAN_OR_EQ = r'\b (' + Equal + r'\s no \s more|(is|was|were)' + Neg + r'\s' + Greater + r') \s than \b'
t_MUL           = r'\b (multiplied \s with|times) \b'
t_MUL_PREF      = r'\b (multiply|the \s product \s of) \b'
t_NOT_CASE      = r'\b it' + SQuote + r's \s not \s the \s case \s that \b'
t_NOT_EQUAL     = r'\b' + Neg + Equal + r'\b'
t_OR            = r'\b or \b'
t_SUB           = r'\b (minus|without) \b'
t_SUB_PREF      = r'\b (subtract|the \s difference \s between) \b'
t_XOR           = r'\b either \b'
   
# Keyword with Multiple Usage #
t_WAS   = r'\b was \b'
t_AND   = r'\b and \b'
t_HAS   = r'\b has \b'
t_IS    = r'\b is \b'
t_BY    = r'\b by \b'
t_FROM  = r'\b from \b'
t_NOT   = r'\b not \b'
t_TO    = r'\b to \b'
   
# Type Name #
t_NUMBER = r'\b' + SPref + r'? \s number \b'
t_NUMBERS = r'\b' + MPref + r'? \s numbers \b'
t_BOOLEAN = r'\b ((the \s)?' + Bool + r'|an \s argument) \b'
t_BOOLEANS = r'\b (the \s)? ' + Bool + r's \b'
t_CHARACTER = r'\b (a \s (' + Char + r')|(the \s)? (' + Str + r'|(' + Char + r')s))\b'
t_STRING = r'\b' + SPref + r'? \s ((' + Char + r')s |' + Str + r') \b'
t_STRINGS = r'\b' + MPref + r'? \s (' + Str + r')s \b'
t_Null = r'\b nothing \b'

# Literals #
t_Number    = r'-? (\d{3} ,?|\d)+ (\. \d+)?'
t_String    = DQuote + String + r'*?' + DQuote
t_Character = SQuote + Char + SQuote
t_True      = r'\b (correct|right|true|yes) \b'
t_False     = r'\b (false|incorrect|no|wrong) \b'

# Ignored #
t_ignore_WHITESPACE = r'\s'
t_ignore_SLComment  = r'(P\.)+ S\. [^\r\n]*'
t_ignore_MLComment  = r'\( [^)]*? \)'

# ID #
# Had to be compact 'cause the lexer first uses the longer expression
# Also for that reason numbers cannot be tagged as ID, though the expression itself is correct
t_ID = r"[\w']+"

# Define a rule so we can track line numbers
def t_Punctuation (t):
    r'[.,?:…‽!]'
    t.lexer.lineno += 1
    return t

# Error handler #
def t_error(t):
    print("Sorry, I cannot understand what '%s' means." % t.value[0])
    t.lexer.skip(1)

# Parsing Code #
lexer = lex.lex(reflags=re.UNICODE)

code = open ("../test/99beer.pony")
lexer.input(code.read())
while True:
     tok = lexer.token()
     if not tok: 
         break
     print(tok)
