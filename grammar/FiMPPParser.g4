/*This file is part of PyFiMPP.
*
*   PyFiMPP is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option)any later version.
*
*   PyFiMPP is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with PyFiMPP.  If not, see <http://www.gnu.org/licenses/>.
*/
parser grammar FiMPPParser;

options
{
    tokenVocab = FiMPPLexer;
}

compilationUnit
    : importDeclaration* typeDeclaration* EOF
    ;

typeDeclaration
    : classDeclaration
    | interfaceDeclaration
    ;

importDeclaration
    : IMPORT classOrInterfaceName PNCT
    ;

classDeclaration
    : superclass superInterfaces? PNCT classOrInterfaceName PNCT
      classBody
      classEnd
    ;

classBody
    : (globalValueDeclaration|methodDeclaration|maneMethodDeclaration)+
    ;

globalValueDeclaration
    : valueDeclarationStatement
    ;

valueDeclarationStatement
    : variableDeclaration
    | constantDeclaration
    ;

variableDeclaration
    : VARIABLE typeName? variableName
    (INITIALIZATION_VARIABLE|OVERLAP_INIT_EQUAL)
    (variable|typeName|methodCall)PNCT
    ;

methodCall
    : methodName (PARAMETER expression ((PNCT expression)* PNCT AND expression)?)?
    ;
    
constantDeclaration
    : VARIABLE typeName? variableName INITIALIZATION_CONSTANT variable PNCT
    ;

variable
    : typeName? expression
    ;

variables
    : variable (AND variable)*
    ;

methodDeclaration
    : PARAGRAPH methodName returnType? (PARAMETER variableNames)? PNCT
      methodBody
      methodEnd
    ;

returnType
    : RETURN_TYPE typeName
    ;

maneMethodDeclaration
    : TODAY methodDeclaration
    ;
    
methodBody
    : block
    ;

methodEnd
    : PARAGRAPH_END methodName PNCT
    ;

interfaceDeclaration
    : classOrInterfaceName PNCT interfaceBody classEnd
    ;

interfaceBody
    : abstractMethodDeclaration
    | constantDeclaration
    ;
    
abstractMethodDeclaration
    : PARAGRAPH methodName (PARAMETER variableNames)? PNCT
    ;
    
superclass
    : REPORT classOrInterfaceName
    ;

superInterfaces
    : (AND superInterface)+
    ;

superInterface
    : classOrInterfaceName
    ;
 
classEnd
    : REPORT_END PNCT variableName PNCT
    ;

typeName
    : NUMBER
    | NUMBERS
    | BOOLEAN
    | BOOLEANS
    | CHARACTER
    | STRING
    | STRINGS
    ;

block
    : statement+
    ;

statement
    : compoundStatement
    | simpleStatement
    ;

simpleStatement
    : valueDeclarationStatement
    | executionStatement
    | returnStatement
    | methodCallStatement
    | printStatement
    | inputStatement
    | promptStatement
    | assignmentStatement
    | increaseStatement
    | decreaseStatement
    ;

compoundStatement
    : ifElseStatement
    | whileStatement
    | doWhileStatement
    | forEachStatement
    | forStatement
    | switchStatement
    ;

executionStatement
    : EXECUTE methodName (PARAMETER variables)? PNCT
    ;

assignmentStatement
    : variableName ASSIGN expression PNCT
    ;

returnStatement
    : RETURN expression PNCT
    ;

methodCallStatement
    : EXECUTE methodCall PNCT
    ;

printStatement
    : PRINT PNCT? expression PNCT
    ;

inputStatement
    : (INPUT|PROMPT)variableName PNCT
    ;

promptStatement
    : PROMPT variableName literal+ PNCT
    ;

ifElseStatement
    : IF_LEFT expression IF_RIGHT? PNCT
      block
      elseStatement?
      END_IF PNCT
    ;

elseStatement
    : ELSE PNCT block
    ;

whileStatement
    : WHILE expression PNCT
      block
      END_SWITCH_WHILE PNCT
    ;

doWhileStatement
    : DO_WHILE PNCT
      block
      END_DO_WHILE expression PNCT
    ;

forStatement
    : FOR_EACH NUMBER variableName FROM expression TO expression PNCT
      block
      END_SWITCH_WHILE PNCT
    ;
    
forEachStatement
    : FOR_EACH (CHARACTER|NUMBER)expression IN expression PNCT
      block
      END_SWITCH_WHILE PNCT
    ;

switchStatement
    : SWITCH expression PNCT
      caseStatement*
      defaultStatement?
      END_SWITCH_WHILE PNCT
    ;
    
caseStatement
    : CASE_LEFT NumberLiteral CASE_RIGHT PNCT
      block
    ;

defaultStatement
    : DEFAULT PNCT
      block
    ;

literal
    : StringLiteral+
    | NumberLiteral+
    | CharacterLiteral+
    | TRUE
    | FALSE
    | NullLiteral
    ;

expression
    : conditionalOrExpression
    ;

conditionalOrExpression
    : conditionalAndExpression
    | conditionalOrExpression OR conditionalAndExpression
    ;

conditionalAndExpression
    : exclusiveOrExpression
    | conditionalAndExpression AND exclusiveOrExpression
    ;

exclusiveOrExpression
    : andExpression
    | XOR_LEFT exclusiveOrExpression OR andExpression
    ;

andExpression
    : relationalExpression
    | andExpression AND relationalExpression
    ;

relationalExpression
    : equalityExpression
    | relationalExpression GREATER_THAN equalityExpression
    | relationalExpression LESS_THAN equalityExpression
    | relationalExpression GREATER_THAN_OR_EQUAL equalityExpression
    | relationalExpression LESS_THAN_OR_EQUAL equalityExpression
    ;

equalityExpression
    : additiveExpression
    | equalityExpression EQUAL additiveExpression
    | equalityExpression NOT_EQUAL additiveExpression
    ;

additiveExpression
    : multiplicativeExpression
    | additiveExpression ADDITION multiplicativeExpression
    | additiveExpression SUBTRACTION multiplicativeExpression
    | ADDITION_LEFT additiveExpression AND multiplicativeExpression
    | SUBTRACTION_LEFT additiveExpression (AND|FROM)multiplicativeExpression
    ;

multiplicativeExpression
    : unaryExpression
    | multiplicativeExpression MULTIPLICATION unaryExpression
    | multiplicativeExpression DIVISION unaryExpression
    | MULTIPLICATION_LEFT multiplicativeExpression (AND|BY)unaryExpression
    | DIVISION_LEFT multiplicativeExpression (AND|FROM)unaryExpression
    ;

unaryExpression
    : INCREMENT_LEFT unaryExpression
    | unaryExpression INCREMENT
    | DECREMENT_LEFT unaryExpression
    | unaryExpression DECREMENT
    | primary +
    ;

increaseStatement
    :
    (INCREMENT_LEFT variableOrMethodName
    | variableOrMethodName INCREMENT
    )PNCT
    ;

decreaseStatement
    :
    (DECREMENT_LEFT variableOrMethodName
    | variableOrMethodName DECREMENT
    )PNCT
    ;

primary
    : literal
    | variableOrMethodName
    ;

classOrInterfaceName
    : name
    ;

variableNames
    : typeName variableName (AND typeName variableName)*
    ;

variableName
    : name
    ;

methodName
    : name
    ;

variableOrMethodName
    : name (PARAMETER expression (AND expression)*)?
    ;

name //getting a name in FiM++ is quite daunting...
    : Identifier (Identifier|Name|TRUE|FALSE|NumberLiteral|NullLiteral|operators)*
    ;

operators
    : XOR_LEFT
    | ADDITION_LEFT
    | SUBTRACTION_LEFT
    | MULTIPLICATION_LEFT
    | DIVISION_LEFT
    | BY
    | FROM
    | TO
    | IN
    ;
