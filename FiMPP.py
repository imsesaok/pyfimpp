# encoding: utf-8
#This file is part of PyFiMPP.
#
#   PyFiMPP is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyFiMPP is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyFiMPP.  If not, see <http://www.gnu.org/licenses/>.
import grammar.tokens
import grammar.parser
import sys

def main(argv):
    input = FileStream(argv[1])
    lexer = FiMPPLexer(input)
    stream = CommonTokenStream(lexer)
    parser = FiMPPParser(stream)
    tree = parser.compilationUnit()
    
    printer = FiMPPParserListener()
    walker = ParseTreeWalker()
    walker.walk (printer, tree)

if __name__ == '__main__' :
    main(sys.argv)
