PyFiM++
=======

PyFiM++(Short for Python Friendship is Magic ++) is WIP Python interpreter for the pony programming language, FiM++. The compiler translates your FiM++ code to Python code and then it runs using magic!
More info: http://fimpp.wikia.com/

Requirements
============
 * Latest python client (Both version 2 and 3 should work OK)
 * Latest PLY library ("pip install ply" or "pip3 install ply" if you're using python 3, or you can also manually download it [here](http://www.dabeaz.com/ply/).)

Tests folder
============

Tests folder include four FiM++ example files: "99beer.pony" (Prints out "99 cider song", pony-style 99 beer song), "helloworld.pony" (classic "Hello World!" program), "howtoadd.pony" (Prints out the sum of 1 to 100), and "100mississipis.pony" (simplified "99 beer song").

PyFiM++ syntax
==============

PyFiM++ syntax is based on FiM++ 1.0, which can be found here: https://docs.google.com/document/d/1gU-ZROmZu0Xitw_pfC1ktCDvJH5rM85TxxQf5pg_xmg/edit

Difference:
* Upon calling a paragraph, combination of punctuation and "and" are used in between the variables, instead of former "and", which can be confused with the operator "and".

License
============

PyFiM++  is distributed under [GNU General Public Licence version 3 or higher](http://www.gnu.org/licenses/gpl-3.0.html).
